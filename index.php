<?php

require_once('animal.php');
require_once('animal1.php');
require_once('animal2.php');

$hewan = new animal("Shaun");
echo "Name : ". $hewan->name . "<br>";
echo "Legs : ". $hewan->legs . "<br>";
echo "Cold blooded : ". $hewan->coldBlooded . "<br><br>";

$katak = new animal1('Buduk');
echo "Name : ". $katak->name . "<br>";
echo "Legs : ". $katak->legs . "<br>";
echo "Cold blooded : ". $katak->coldBlooded . "<br>";
echo "Jump : ". $katak->action . "<br><br>";

$ape = new animal2('Kera Sakti');
echo "Name : ". $ape->name . "<br>";
echo "Legs : ". $ape->legs . "<br>";
echo "Cold blooded : ". $ape->coldBlooded . "<br>";
echo "Yell : ". $ape->action . "<br>";
?>